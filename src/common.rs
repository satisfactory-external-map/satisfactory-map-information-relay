use num_traits::Num;
use serde::{Serialize, Deserialize};

#[derive(Copy, Clone, Serialize, Deserialize, Debug)]
pub struct Vector4<T: Num> {
    pub x: T,
    pub y: T,
    pub z: T,
    pub w: T
}

#[derive(Copy, Clone, Serialize, Deserialize, Debug)]
pub struct Vector3<T: Num> {
    pub x: T,
    pub y: T,
    pub z: T
}

#[derive(Copy, Clone, Serialize, Deserialize, Debug)]
pub struct Vector2<T: Num> {
    pub x: T,
    pub y: T
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct PlayerBlip {
    pub position: Vector2<f32>,
    pub heading: f32,
    pub name: String
}
