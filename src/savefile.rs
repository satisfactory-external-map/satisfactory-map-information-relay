use crate::common::{
    PlayerBlip,
    Vector2,
    Vector3,
    Vector4
};

use std::convert::TryInto;
use std::{
    fs::File,
    io::{
        Cursor,
        prelude::*
    }
};
use byteorder::{ReadBytesExt, LittleEndian};
use flate2::read::ZlibDecoder;


trait TypedSavedObject {
    fn get_type_path(&self) -> &str;
    fn get_root_object_name(&self) -> &str;
    fn get_instance_name(&self) -> &str;
}

struct SaveEntity {
    type_path: String,
    root_object_name: String,
    instance_name: String
}

struct SaveComponent {
    type_path: String,
    root_object_name: String,
    instance_name: String
}


impl TypedSavedObject for SaveEntity {
    fn get_type_path(&self) -> &str { &self.type_path }
    fn get_root_object_name(&self) -> &str { &self.root_object_name }
    fn get_instance_name(&self) -> &str { &self.instance_name }
}

impl TypedSavedObject for SaveComponent {
    fn get_type_path(&self) -> &str { &self.type_path }
    fn get_root_object_name(&self) -> &str { &self.root_object_name }
    fn get_instance_name(&self) -> &str { &self.instance_name }
}

trait ShadowTypedSavedObject {
    fn get_type_path(&self) -> &str;
    fn get_root_object_name(&self) -> &str;
    fn get_instance_name(&self) -> &str;
}

struct ShadowSaveEntity {
    type_path: String,
    root_object_name: String,
    instance_name: String,
    rotation: Vector4<f32>,
    position: Vector3<f32>
}

impl TypedSavedObject for ShadowSaveEntity {
    fn get_type_path(&self) -> &str { &self.type_path }
    fn get_root_object_name(&self) -> &str { &self.root_object_name }
    fn get_instance_name(&self) -> &str { &self.instance_name }
}

enum ShadowSavedObject {
    Entity(ShadowSaveEntity),
    Component(SaveComponent)
}

pub fn get_blips_for_savefile(path: &str) -> Result<Vec<PlayerBlip>, String> {
    let savefile_bytes = load_savefile(path)?;
    let blips = extract_blips(&savefile_bytes);
    if blips.is_ok() {
        let blips = blips.unwrap();
        return Ok(blips);
    }
    let err = blips.err().unwrap();
    eprintln!("Unable to extract player blips ({})", &err);
    Ok(Vec::new())
}

fn load_savefile(path: &str) -> Result<Vec<u8>,String> {
    let res = File::open(&path);
    if res.is_err() {
        return Err("Unable to open input file".to_string());
    }
    let mut file = res.unwrap();
    let mut contents: Vec<u8> = Vec::new();
    let res = file.read_to_end(&mut contents);
    if res.is_err() {
        return Err("Unable to read input file".to_string());
    }
    Ok(contents)
}

fn extract_blips(bytes: &Vec<u8>) -> Result<Vec<PlayerBlip>, &str> {
    let mut cursor = Cursor::new(bytes);

    let save_header_version = read_i32(&mut cursor);
    if save_header_version != 6 {
        return Err("Unsupported header version");
    }

    let save_version = read_i32(&mut cursor);
    if save_version < 21 {
        return Err("Unsupported savefile version");
    }

    let _build_version = read_i32(&mut cursor);
    let _map_name = read_string(&mut cursor);
    let _map_options = read_string(&mut cursor);
    let _session_name = read_string(&mut cursor);
    let _play_duration = read_i32(&mut cursor);
    let _save_datetime = read_i64(&mut cursor);

    if save_header_version >= 6 {
        // read session visibility byte
        cursor.set_position(cursor.position() + 1);
    }

    let mut uncompressed_size: usize = 0;
    let mut chunks: Vec<Vec<u8>> = Vec::new();
    let maximum_header_position: u64 = (bytes.len() - 48).try_into().unwrap();
    while cursor.position() < maximum_header_position {
        // read chunk header
        read_i64(&mut cursor);
        read_i64(&mut cursor);

        read_i64(&mut cursor);
        let chunk_uncompressed_size = read_i64(&mut cursor);

        let subchunk_compressed_size = read_i64(&mut cursor);
        let subchunk_uncompressed_size = read_i64(&mut cursor);

        if subchunk_uncompressed_size != chunk_uncompressed_size {
            return Err("Invalid sizes");
        }

        let start_position = cursor.position() as usize;
        let target_position = start_position + (subchunk_compressed_size as usize);
        let slice = &cursor.get_ref()[start_position .. target_position];
        let slice = slice.to_vec();

        chunks.push(slice);
        cursor.set_position(target_position as u64);
        uncompressed_size += subchunk_uncompressed_size as usize;
    }

    let mut decompressed_bytes: Vec<u8> = Vec::with_capacity(uncompressed_size);
    let mut decompressed_size: usize = 0;
    for chunk in chunks {
        let slice = &chunk[..];
        let mut decoder = ZlibDecoder::new(slice);
        let mut current_slice_buffer: Vec<u8> = Vec::new();
        let res = decoder.read_to_end(&mut current_slice_buffer);
        if res.is_err() {
            return Err("Unable to decompress");
        }
        let decompress_count = res.unwrap();
        decompressed_size += decompress_count;
        decompressed_bytes.append(&mut current_slice_buffer);
    }

    let mut cursor = Cursor::new(&decompressed_bytes);
    let data_length = read_i32(&mut cursor);
    if data_length + 4 != decompressed_size as i32 {
        return Err("Invalid decompressed byte count");
    }

    let total_saved_object_header_count = read_u32(&mut cursor);
    let mut saved_object_headers: Vec<ShadowSavedObject> = Vec::new();
    for _ in 0 .. total_saved_object_header_count {
        let r#type = read_i32(&mut cursor);
        let type_path = read_string(&mut cursor);
        let root_object = read_string(&mut cursor);
        let instance_name = read_string(&mut cursor);
        match r#type {
            0 => {
                // component
                read_string(&mut cursor);
                saved_object_headers.push(
                    ShadowSavedObject::Component(
                        SaveComponent {
                            type_path: type_path,
                            root_object_name: root_object,
                            instance_name: instance_name
                        }
                    )
                );
            },
            1 => {
                // entity
                read_i32(&mut cursor);
                let rotation = read_vec4(&mut cursor);
                let position = read_vec3(&mut cursor);
                read_vec3(&mut cursor);
                read_i32(&mut cursor);
                saved_object_headers.push(
                    ShadowSavedObject::Entity(
                        ShadowSaveEntity {
                            type_path: type_path,
                            root_object_name: root_object,
                            instance_name: instance_name,
                            rotation: rotation,
                            position: position,
                        }
                    )
                )
            },
            _ => {
                return Err("Unsupported type");
            }
        }
    }

    let mut player_blips: Vec<PlayerBlip> = Vec::new();
    for saved_object_header in &saved_object_headers {
        match saved_object_header {
            ShadowSavedObject::Component(_) => { continue; },
            ShadowSavedObject::Entity(ent) => {
                let r#type = ent.get_type_path();
                let name = String::from(ent.get_instance_name());
                let required = "/Game/FactoryGame/Character/Player/Char_Player";
                if ! r#type.contains(required) {
                    continue;
                }
                let name = &name[(required.len() + 1)..];
                let pos = ent.position;
                let rot = ent.rotation;
                let blip = PlayerBlip {
                    name: String::from(name),
                    position: Vector2 {
                        x: pos.x,
                        y: pos.y
                    },
                    heading: rot.z
                };
                player_blips.push(blip);
            }
        };
    }

    Ok(player_blips)

}

fn read_i32(cursor: &mut Cursor<&Vec<u8>>) -> i32 {
    cursor.read_i32::<LittleEndian>().unwrap()
}

fn read_u32(cursor: &mut Cursor<&Vec<u8>>) -> u32 {
    cursor.read_u32::<LittleEndian>().unwrap()
}

fn read_i64(cursor: &mut Cursor<&Vec<u8>>) -> i64 {
    cursor.read_i64::<LittleEndian>().unwrap()
}

fn read_f32(cursor: &mut Cursor<&Vec<u8>>) -> f32 {
    cursor.read_f32::<LittleEndian>().unwrap()
}

fn read_vec3(cursor: &mut Cursor<&Vec<u8>>) -> Vector3<f32> {
    let x = read_f32(cursor);
    let y = read_f32(cursor);
    let z = read_f32(cursor);
    Vector3 {
        x: x,
        y: y,
        z: z
    }
}

fn read_vec4(cursor: &mut Cursor<&Vec<u8>>) -> Vector4<f32> {
    let x = read_f32(cursor);
    let y = read_f32(cursor);
    let z = read_f32(cursor);
    let w = read_f32(cursor);
    Vector4 {
        x: x,
        y: y,
        z: z,
        w: w
    }
}

fn read_string(cursor: &mut Cursor<&Vec<u8>>) -> String {
    let mut string_length = read_i32(cursor);
    if string_length < 0 {
        string_length = string_length * -2;
    }

    // read chars
    let current_position = cursor.position() as usize;
    let target_position = current_position + (string_length as usize);
    let slice = &cursor.get_ref()[current_position .. target_position];
    let read = String::from_utf8(slice.to_vec()).expect("Unable to read string");
    
    cursor.set_position(target_position as u64);

    read
}