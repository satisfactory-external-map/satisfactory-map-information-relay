extern crate satisfactory_save_explorer;
use satisfactory_save_explorer::savefile::get_blips_for_savefile;

use dirs::*;
use walkdir::WalkDir;
use std::{
    fs,
    time::{
        SystemTime,
        Duration
    },
    thread::sleep
};
use clap::{
    App,
    Arg
};
use reqwest::Client;


#[derive(PartialEq, Eq, Debug, Clone)]
struct SavefileInfo {
    path: String,
    modified: SystemTime
}

impl PartialOrd for SavefileInfo {
    fn partial_cmp(&self, other: &Self) -> std::option::Option<std::cmp::Ordering> {
        if self.path == other.path && self.modified == other.modified {
            return Some(std::cmp::Ordering::Equal);
        }

        Some(self.modified.cmp(&other.modified))
    }
}

impl Ord for SavefileInfo {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.modified.partial_cmp(&other.modified).unwrap()
    }
}

async fn process_and_upload(savefile_info: &SavefileInfo, target: &str) -> Result<(), String> {
    let blips = get_blips_for_savefile(&savefile_info.path)?;

    println!("Sending savefile");
    let res = Client::new()
        .post(target)
        .json(&blips)
        .send()
        .await;

    if res.is_err() {
        let res = res.err().unwrap();
        return Err(format!("Sending savefile failed ({})", &res));
    }
    println!("Sent");

    Ok(())
}

#[tokio::main]
async fn main() {
    let args = App::new("SEDST (Satisfactory External Dynmap Savefile Transmitter)")
        .arg(
            Arg::with_name("target")
            .index(1)
            .help("Target URI (e.g. http://torsten.cz:29007/submit/1)")
            .required(true)
        ).get_matches();
    let target: String = args.value_of("target").expect("Missing target URI").to_string();

    let root_search_path = data_local_dir();
    if root_search_path.is_none() {
        eprintln!("Unable to find local app data");
        return;
    }
    let root_search_path = root_search_path.unwrap();
    let root_search_path = String::from(root_search_path.to_str().unwrap()) + "/FactoryGame/Saved/SaveGames";

    println!("{:#?}", data_local_dir());

    let mut last_save_info: Option<SavefileInfo> = None;
    loop {
        // fetch newest savefile
        let mut entries = WalkDir::new(&root_search_path).follow_links(false).same_file_system(true).max_depth(3).into_iter().map(|entry| {
                if entry.is_err() {
                    return None;
                }
        
                let entry = entry.unwrap();
        
                let file_type = entry.file_type();
                let is_file = file_type.is_file();
                if !is_file {
                    return None;
                }
                let path = entry.path().as_os_str().to_str().unwrap();
                let path = path[2..].to_string().replace("\\", "/");
                let res = fs::metadata(&path);
                if res.is_err() {
                    return None;
                }
                let res = res.unwrap().modified();
                if res.is_err() {
                    return None;
                }
                let modified = res.unwrap();
                if path.contains(".sav") {
                    let info = SavefileInfo {
                        path: path,
                        modified: modified
                    };

                    return Some(info);
                }
                
                return None;
            }
        ).filter(|el| el.is_some()).map(|el| el.unwrap()).collect::<Vec<SavefileInfo>>();
        entries.sort();

        if entries.len() == 0 {
            // wait and continue
            sleep(Duration::from_secs(5));
            continue;
        }

        let newest_entry = entries[entries.len() - 1].clone();

        if last_save_info.is_some() {
            let save_info = last_save_info.as_ref().unwrap().clone();
            if save_info != newest_entry {
                // got a new entry
                println!("Got newer savefile");
                last_save_info = Some(newest_entry.clone());
                let res = process_and_upload(&newest_entry, &target).await;
                if res.is_err() {
                    eprintln!("{}", res.err().unwrap());
                    continue;
                }
            }
        } else {
            println!("Got first savefile");
            last_save_info = Some(newest_entry.clone());
            let res = process_and_upload(&newest_entry, &target).await;
            if res.is_err() {
                eprintln!("{}", res.err().unwrap());
                continue;
            }
        }
    }
}