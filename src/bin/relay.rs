extern crate satisfactory_save_explorer;
use satisfactory_save_explorer::common::PlayerBlip;

use warp::{
    self,
    Filter,
    reply::Reply,
    http::StatusCode
};
use serde::{Serialize, Deserialize};
use std::{
    sync::{
        Arc,
        Mutex
    },
    time::{
        Duration,
        Instant
    },
    cell::{
        RefCell,
        RefMut
    }
};
use bytes::Bytes;

#[derive(Clone, Debug)]
struct Savefile {
    id: u64,
    blips: Vec<TimestampedPlayerBlip>,
    savebytes: Option<Bytes>
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct SerializableSavefile {
    id: u64,
    blips: Vec<PlayerBlip>
}

impl From<Savefile> for SerializableSavefile {
    fn from(item: Savefile) -> Self {
        let blips = item.blips.iter().map(|timestamped_blip| {
            if timestamped_blip.timestamp.elapsed() > Duration::from_secs(10) {
                return None;
            } {
                return Some((&timestamped_blip.blip).clone());
            }
        }).filter(|el| el.is_some()).map(|el| el.unwrap()).collect::<Vec<PlayerBlip>>();
        Self {
            id: item.id,
            blips: blips
        }
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
struct StatisticsReply {
    count: usize
}

#[derive(Clone, Debug)]
struct TimestampedPlayerBlip {
    timestamp: Instant,
    blip: PlayerBlip
}

impl From<PlayerBlip> for TimestampedPlayerBlip {
    fn from(item: PlayerBlip) -> Self {
        Self {
            timestamp: Instant::now(),
            blip: item
        }
    }
}

#[tokio::main]
async fn main() {
    let savefiles = Arc::new(Mutex::new(RefCell::new(Vec::<Savefile>::new())));

    let submit_blips = savefiles.clone();
    let submit_savefiles = savefiles.clone();
    let retrieve_blips = savefiles.clone();
    let retrieve_savefiles = savefiles.clone();
    let statistics_savefiles = savefiles.clone();

    let submit = warp::post()
        .and(warp::path("submit"))
        .and(warp::path::param::<u64>())
        .and(warp::path::end())
        .and(warp::body::content_length_limit(1024 * 32))
        .and(warp::body::json())
        .map(move |id, blips: Vec<PlayerBlip>| {
            // savefile automagically deserialized and ready to use
            // along with the id param
            let savefiles = submit_blips.lock().unwrap();
            let blip_length = &blips.len();
            let blip_names = blips.clone().iter().map(|el| el.name.clone()).collect::<Vec<String>>();
            let new_savefiles: Vec<Savefile> = Vec::new();
            let original_savefiles = savefiles.replace(new_savefiles);
            {
                let mut new_savefiles = savefiles.borrow_mut();
                let mut modified_previous: bool = false;
                for savefile_owned in original_savefiles.into_iter() {
                    let mut new_blips = savefile_owned.blips;
                    if savefile_owned.id == id {
                        modified_previous = true;
                        println!("Updating previous blips");
                        for blip in &blips{
                            new_blips.retain(|og_blip| og_blip.blip.name != blip.name);
                            new_blips.push(TimestampedPlayerBlip {
                                timestamp: Instant::now(),
                                blip: blip.clone()
                            });
                        }
                    }
                    new_savefiles.push(Savefile {
                        id: savefile_owned.id,
                        blips: new_blips,
                        savebytes: savefile_owned.savebytes
                    });
                }
                if !modified_previous {
                    println!("Creating new session from blips");
                    new_savefiles.push(Savefile {
                        id: id,
                        blips: blips.iter().map(|blip| blip.clone().into()).collect::<Vec<TimestampedPlayerBlip>>(),
                        savebytes: None
                    });
                }
            }
            println!("Received players with ID {} and {} blips ({}), {} savefiles in memory", &id, &blip_length, blip_names.join(", "), { savefiles.borrow_mut().len() });
            warp::reply()
        });

    let submit_save = warp::post()
        .and(warp::path("submit"))
        .and(warp::path::param::<u64>())
        .and(warp::path("savefile"))
        .and(warp::path::end())
        .and(warp::body::content_length_limit(1024 * 1024 * 32)) // limit accepted savefile to 32 megs
        .and(warp::body::bytes())
        .map(move |id, bytes: Bytes| {
            let savefiles = submit_savefiles.lock().unwrap();
            let byte_length = &bytes.len();
            let new_savefiles: Vec<Savefile> = Vec::new();
            let original_savefiles = savefiles.replace(new_savefiles);
            {
                let mut new_savefiles = savefiles.borrow_mut();
                let mut modified_previous: bool = false;
                for savefile_owned in original_savefiles.into_iter() {
                    let mut new_savebytes: Option<Bytes> = savefile_owned.savebytes;
                    if savefile_owned.id == id {
                        println!("Updating previous savefile");
                        modified_previous = true;
                        new_savebytes = Some(bytes.clone());
                    }
                    new_savefiles.push(Savefile {
                        id: savefile_owned.id,
                        blips: savefile_owned.blips,
                        savebytes: new_savebytes
                    });
                }
                if !modified_previous {
                    println!("Creating new session from savefile");
                    new_savefiles.push(Savefile {
                        id: id,
                        blips: Vec::new(),
                        savebytes: Some(bytes)
                    });
                }
            }
            println!("Received savefile with ID {} and {} bytes", &id, &byte_length);
            warp::reply()
        });
        
    let retrieve = warp::get()
        .and(warp::path("retrieve"))
        .and(warp::path::param::<u64>())
        .and(warp::path::end())
        .map(move |id| {
            let savefiles = retrieve_blips.lock().unwrap();
            let savefiles = savefiles.borrow_mut();
            let located = savefiles.iter().find(|el| el.id == id);
            if located.is_none() {
                println!("No savefile with ID {}", &id);
                return StatusCode::NOT_FOUND.into_response();
            }
            let located: SerializableSavefile = located.unwrap().clone().into();
            println!("Returning blips for session ID {} and {} blips ({})", &located.id, &located.blips.len(), (&located.blips).clone().iter().map(|el| el.name.clone()).collect::<Vec<String>>().join(", "));
            warp::reply::json(&located).into_response()
        });

    let retrieve_save = warp::get()
        .and(warp::path("retrieve"))
        .and(warp::path::param::<u64>())
        .and(warp::path("savefile"))
        .and(warp::path::end())
        .map(move |id| {
            let savefiles = retrieve_savefiles.lock().unwrap();
            let savefiles = savefiles.borrow_mut();
            let located = savefiles.iter().find(|el| el.id == id);
            if located.is_none() {
                println!("No session with ID {}", &id);
                return StatusCode::NOT_FOUND.into_response();
            }
            let located: Savefile = located.unwrap().clone();
            let savebytes = located.savebytes;
            if savebytes.is_none() {
                println!("No savefile for session ID {}", &id);
                return StatusCode::NOT_FOUND.into_response();
            }
            let savebytes = savebytes.unwrap();
            let byte_count = &savebytes.len();
            println!("Returning savefile for session ID {} with {} bytes", &id, &byte_count);
            let response_vec = savebytes.to_vec();
            response_vec.into_response()
        });        

    let statistics = warp::get()
        .and(warp::path::end())
        .map(move || {
            let savefiles = statistics_savefiles.lock().unwrap();
            let savefiles = savefiles.borrow_mut();
            let reply = StatisticsReply {
                count: savefiles.len()
            };
            warp::reply::json(&reply)
        });

    let routes = submit_save.or(submit).or(retrieve_save).or(retrieve).or(statistics);

    let port = 29007;
    println!("Listening on port {}", &port);
    warp::serve(routes).run(([0, 0, 0, 0], port)).await

}